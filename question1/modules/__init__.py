from re import A


def open_menu():
    option = float(input('''
Qual opção você quer?
[1] Cadastrar novo boi
[2] Calcular o boi mais gordo
[3] Calcular o boi mais magro))
Digite uma opção: '''))
    print()
    if option == 1:
        return "new_ox"
    elif option == 2:
        return "fat_ox"
    elif option == 3:
        return "skinny_ox"
    else:
        return "erro!"


def register_ox():
    new_ox = {}
    new_ox["name"] = input("Digite o nome do boi: ")
    new_ox["mass"] = float(input("Digite a massa do boi: "))
    list_of_ox = open("list_of_ox.txt", 'a')
    list_of_ox.write(f'{new_ox["name"]};{new_ox["mass"]}\n')

def create_array_of_oxen():
    array_of_oxen = []
    list_of_oxen = open("list_of_ox.txt")
    lines_of_oxen = list_of_oxen.readlines()
    for lines in lines_of_oxen:
        temp_dict = {}
        name, mass = lines.split(';')
        mass = float(mass)
        temp_dict["name"] = name
        temp_dict["mass"] = mass
        array_of_oxen.append(temp_dict)
    return array_of_oxen


def show_fat_ox():
    array_of_oxen = create_array_of_oxen()
    fat_ox = 0
    name_fat_ox = ''
    for i in array_of_oxen:
        if i["mass"] >= fat_ox:
            fat_ox = i["mass"]
            name_fat_ox = i["name"]
    print(f"Nome do boi: {name_fat_ox}")
    print(f"Massa do boi: {fat_ox:.2f}") 



def show_skinny_ox():
    array_of_oxen = create_array_of_oxen()
    skinny_ox = 10**10
    name_skinny_ox = ''
    for i in array_of_oxen:
        if i["mass"] <= skinny_ox:
            skinny_ox = i["mass"]
            name_skinny_ox = i["name"]
    print(f"Nome do boi: {name_skinny_ox}")
    print(f"Massa do boi: {skinny_ox:.2f}") 


